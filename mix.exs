defmodule ExampleGitlabCiEx.MixProject do
  use Mix.Project

  def project do
    [
      app: :example_gitlab_ci_ex,
      version: "0.1.0",
      elixir: "~> 1.10",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      releases: [
        example_gitlab_ci_ex: [
          steps: [:assemble, :tar],
          applications: [runtime_tools: :permanent],
          include_executables_for: [:unix]
        ]
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
      {:junit_formatter, "~> 3.1", only: [:test]}
    ]
  end
end
