defmodule ExampleGitlabCiEx do
  @moduledoc """
  Documentation for `ExampleGitlabCiEx`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> ExampleGitlabCiEx.hello()
      :world

  """
  def hello do
    :world
  end
end
